(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([], factory);
  }
  else if (typeof exports === 'object') {
    module.exports = factory();
  }
  else {
    root.beaufort = factory();
  }
}(this, function() {
	'use strict';

	var windNames = [
			"calm",
			"light air",
			"light breeze",
			"gentle breeze",
			"moderate breeze",
			"fresh breeze",
			"strong breeze",
			"near gale",
			"gale",
			"severe gale",
			"storm",
			"violent storm",
			"hurricane"
			];

	var getWindScale = function(speedKnots){

		if(speedKnots<0) return undefined;

		var windSpeeds = [0.99, 3,6,10,16,21,27,33,40,47,55,63];

		if(speedKnots == undefined || typeof speedKnots != 'number') return undefined;

		 var beaufortScale = windSpeeds.reduce(function(previousValue, currentValue, index, array){
		 	if(speedKnots>currentValue) previousValue++;
		 	return previousValue;
		 },0);

		 return beaufortScale;
	}

	var getWindName = function(windScale){
		if(typeof windScale == 'number') windScale = getWindScale(windScale);
		return windNames[windScale];
	}

	return {
	   getWindScale: getWindScale,
	   getWindName: getWindName
	}

	console.log(getWindScale(3));

}));


